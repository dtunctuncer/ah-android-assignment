@file:Suppress("UnstableApiUsage")

pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    includeBuild("build-logic")
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
rootProject.name = "ArtObjects"
include(":app")
include(":core")
include(":core:common")
include(":core:testing")
include(":core:model")
include(":core:network")
include(":core:data")
include(":core:domain")
include(":feature")
include(":feature:art-objects")
include(":feature:art-object-detail")
