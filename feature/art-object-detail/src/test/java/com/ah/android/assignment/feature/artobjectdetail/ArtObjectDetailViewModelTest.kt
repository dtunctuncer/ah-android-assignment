package com.ah.android.assignment.feature.artobjectdetail

import androidx.lifecycle.SavedStateHandle
import app.cash.turbine.test
import com.ah.android.assignment.data.repository.collections.CollectionsRepository
import com.ah.android.assignment.testing.util.MainDispatcherRule
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.instanceOf
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class ArtObjectDetailViewModelTest {
    private val testDispatcher = UnconfinedTestDispatcher()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule(testDispatcher)

    private lateinit var collectionsRepository: CollectionsRepository
    private lateinit var sut: ArtObjectDetailViewModel
    private lateinit var savedStateHandle: SavedStateHandle

    @Before
    fun setUp() {
        savedStateHandle = SavedStateHandle(mapOf("objectNumber" to "kasdaksdas"))
        collectionsRepository = mockk()
        every { collectionsRepository.getArtObjectDetail(any()) } returns flowOf(mockk())
        sut = ArtObjectDetailViewModel(collectionsRepository, savedStateHandle)
    }

    @Test
    fun `uiState should be loading initially`() = runTest(testDispatcher) {
        assertEquals(
            ArtObjectDetailUiState.Loading,
            sut.artObjectDetailUiState.value
        )
    }

    @Test
    fun `uiState should be error when there is no object number`() = runTest(testDispatcher) {
        savedStateHandle = SavedStateHandle()
        collectionsRepository = mockk()
        sut = ArtObjectDetailViewModel(collectionsRepository, savedStateHandle)

        sut.artObjectDetailUiState.test {
            assertThat(awaitItem(), instanceOf(ArtObjectDetailUiState.Error::class.java))
        }
    }

    @Test
    fun `uiState should be success when there is object number`() = runTest(testDispatcher) {
        sut.artObjectDetailUiState.test {
            assertThat(awaitItem(), instanceOf(ArtObjectDetailUiState.Success::class.java))
        }
    }
}