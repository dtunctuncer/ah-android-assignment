package com.ah.android.assignment.feature.artobjectdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import coil.load
import com.ah.android.assignment.common.exceptionhandling.ExceptionHandling
import com.ah.android.assignment.feature.artobjectdetail.databinding.FragmentArtObjectDetailBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ArtObjectDetailFragment : Fragment() {

    private val viewModel by viewModels<ArtObjectDetailViewModel>()
    private var _binding: FragmentArtObjectDetailBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var exceptionHandling: ExceptionHandling
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentArtObjectDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.artObjectDetailClose.setOnClickListener { findNavController().navigateUp() }
        collectUiState()
    }

    private fun collectUiState() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                viewModel.artObjectDetailUiState.collect(::renderUiState)
            }
        }
    }

    private fun renderUiState(state: ArtObjectDetailUiState) {
        binding.artObjectDetailProgressBar.isVisible =
            state is ArtObjectDetailUiState.Loading
        when (state) {
            is ArtObjectDetailUiState.Error -> state.throwable?.let { throwable ->
                exceptionHandling.handleException(requireActivity(), throwable)
            }

            ArtObjectDetailUiState.Loading -> {
                // no - op
            }

            is ArtObjectDetailUiState.Success -> {
                val artObjectDetail = state.artObjectDetail
                binding.artObjectDetailImage.load(artObjectDetail.imageUrl, builder = {
                    error(com.ah.android.assignment.core.common.R.drawable.ic_broken_image)
                    placeholder(com.ah.android.assignment.core.common.R.drawable.ic_image)
                })
                binding.artObjectDetailTitle.text = artObjectDetail.title
                binding.artObjectDetailSubtitle.text = artObjectDetail.maker
                binding.artObjectDetailDescription.text = artObjectDetail.description
            }
        }
    }
}
