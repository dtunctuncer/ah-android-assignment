package com.ah.android.assignment.feature.artobjectdetail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ah.android.assignment.common.result.Result
import com.ah.android.assignment.common.result.asResult
import com.ah.android.assignment.data.repository.collections.CollectionsRepository
import com.ah.android.assignment.model.collection.ArtObjectDetail
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

@HiltViewModel
class ArtObjectDetailViewModel @Inject constructor(
    collectionsRepository: CollectionsRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    val artObjectDetailUiState: StateFlow<ArtObjectDetailUiState> = artObjectDetailUiState(collectionsRepository, savedStateHandle)
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5_000), ArtObjectDetailUiState.Loading)


    private fun artObjectDetailUiState(
        collectionsRepository: CollectionsRepository,
        savedStateHandle: SavedStateHandle
    ): Flow<ArtObjectDetailUiState> {
        val objectNumber = savedStateHandle.get<String>("objectNumber")
            ?: return flowOf(ArtObjectDetailUiState.Error(IllegalStateException("Object number is not available")))
        return collectionsRepository.getArtObjectDetail(objectNumber)
            .asResult()
            .map { result ->
                when (result) {
                    is Result.Error -> ArtObjectDetailUiState.Error(result.exception)
                    Result.Loading -> ArtObjectDetailUiState.Loading
                    is Result.Success -> ArtObjectDetailUiState.Success(result.data)
                }
            }
    }
}

sealed interface ArtObjectDetailUiState {
    object Loading : ArtObjectDetailUiState
    data class Error(val throwable: Throwable?) : ArtObjectDetailUiState
    data class Success(val artObjectDetail: ArtObjectDetail) : ArtObjectDetailUiState
}