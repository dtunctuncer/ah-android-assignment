package com.ah.android.assignment.feature.artobjectdetail

import androidx.core.os.bundleOf
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.ah.android.assignment.core.common.R
import com.ah.android.assignment.data.di.DataModule
import com.ah.android.assignment.data.repository.collections.CollectionsRepository
import com.ah.android.assignment.model.collection.ArtObjectDetail
import com.ah.android.assignment.testing.launchFragmentInHiltContainer
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
@UninstallModules(DataModule::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class ArtObjectDetailFragmentTest {
    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @BindValue
    @JvmField
    val collectionsRepository: CollectionsRepository = mockk()


    private val artObjectDetail = ArtObjectDetail(
        objectNumber = "objectNumber",
        title = "title",
        longTitle = "longTitle",
        subtitle = "subtitle",
        imageUrl = "imageUrl",
        description = "description",
        objectTypes = "objectTypes",
        objectCollection = "objectCollection",
        maker = "maker",
        materials = "materials",
    )

    @Before
    fun setUp() {
        every { collectionsRepository.getArtObjectDetail(any()) } returns flowOf(artObjectDetail)
        hiltRule.inject()
    }

    @Test
    fun shouldShowArtObjectDetail() {
        launchFragmentInHiltContainer<ArtObjectDetailFragment>(
            fragmentArgs = bundleOf("objectNumber" to "test")
        )

        artObjectsDetail {
            isArtObjectDetailShown(title = artObjectDetail.title)
        }
    }

    @Test
    fun shouldShowErrorMessageWhenThereIsError() {
        every { collectionsRepository.getArtObjectDetail(any()) } returns flow { error("error") }

        launchFragmentInHiltContainer<ArtObjectDetailFragment>(fragmentArgs = bundleOf("objectNumber" to "test"))

        artObjectsDetail {
            isErrorMessageShown(R.string.unknown_error)
        }
    }

    @Test
    fun shouldShowErrorMessageWhenThereIsNoObjectNumber() {
        every { collectionsRepository.getArtObjectDetail(any()) } returns flow { error("error") }

        launchFragmentInHiltContainer<ArtObjectDetailFragment>()

        artObjectsDetail {
            isErrorMessageShown(R.string.unknown_error)
        }
    }
}