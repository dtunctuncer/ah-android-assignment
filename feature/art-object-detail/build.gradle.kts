plugins {
    id("com.ah.android.assignment.library.feature")
}

android {
    namespace = "com.ah.android.assignment.feature.artobjectdetail"
    packaging {
        resources.excludes.add("META-INF/*")
    }
}