package com.ah.android.assignment.feature.artobjects

import app.cash.turbine.test
import com.ah.android.assignment.testing.util.MainDispatcherRule
import com.ah.android.domain.GetCollectionsUseCase
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.instanceOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ArtObjectsViewModelTest {
    private val testDispatcher = UnconfinedTestDispatcher()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule(testDispatcher)

    private lateinit var fakeCollectionsRepository: FakeCollectionsRepository
    private lateinit var getCollectionsUseCase: GetCollectionsUseCase
    private lateinit var sut: ArtObjectsViewModel


    @Before
    fun setUp() {
        fakeCollectionsRepository = FakeCollectionsRepository()
        getCollectionsUseCase = GetCollectionsUseCase(fakeCollectionsRepository)
        sut = ArtObjectsViewModel(getCollectionsUseCase)
    }

    @Test
    fun `uiState should be loading initially`() = runTest(testDispatcher) {
        assertEquals(
            ArtObjectsUiState.Loading,
            sut.artObjectsUiState.value
        )
    }

    @Test
    fun `uiState should be success when there is no exception`() = runTest(testDispatcher) {
        sut.artObjectsUiState.test {
            val state = awaitItem()
            assertThat(state, instanceOf(ArtObjectsUiState.Success::class.java))
        }
    }
}
