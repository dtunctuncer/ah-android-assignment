package com.ah.android.assignment.feature.artobjects

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.ah.android.assignment.common.result.Result
import com.ah.android.assignment.common.result.asResult
import com.ah.android.assignment.model.collection.Collection
import com.ah.android.domain.GetCollectionsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

@HiltViewModel
class ArtObjectsViewModel @Inject constructor(
    getCollectionsUseCase: GetCollectionsUseCase
) : ViewModel() {

    val artObjectsUiState: StateFlow<ArtObjectsUiState> = artObjectsUiState(getCollectionsUseCase)
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5_000), ArtObjectsUiState.Loading)

    private fun artObjectsUiState(getCollectionsUseCase: GetCollectionsUseCase): Flow<ArtObjectsUiState> {
        return getCollectionsUseCase()
            .cachedIn(viewModelScope)
            .asResult()
            .map { result ->
                when (result) {
                    is Result.Error -> ArtObjectsUiState.Error(result.exception)
                    Result.Loading -> ArtObjectsUiState.Loading
                    is Result.Success -> ArtObjectsUiState.Success(result.data)
                }
            }
    }
}

sealed interface ArtObjectsUiState {
    object Loading : ArtObjectsUiState
    data class Error(val throwable: Throwable?) : ArtObjectsUiState
    data class Success(val collections: PagingData<Collection>) : ArtObjectsUiState
}
