package com.ah.android.assignment.feature.artobjects

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ah.android.assignment.feature.artobjects.databinding.ItemCollectionsLoadStateFooterViewBinding

class CollectionsLoadStateAdapter(
    private val retry: () -> Unit
) : LoadStateAdapter<CollectionsLoadStateViewHolder>() {
    override fun onBindViewHolder(holder: CollectionsLoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): CollectionsLoadStateViewHolder {
        return CollectionsLoadStateViewHolder.create(parent, retry)
    }
}

class CollectionsLoadStateViewHolder(
    private val binding: ItemCollectionsLoadStateFooterViewBinding,
    retry: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.loadStateRetryButton.setOnClickListener { retry.invoke() }
    }

    fun bind(loadState: LoadState) {
        if (loadState is LoadState.Error) {
            binding.loadStateErrorMessage.text = loadState.error.localizedMessage
        }
        binding.loadStateProgressBar.isVisible = loadState is LoadState.Loading
        binding.loadStateRetryButton.isVisible = loadState is LoadState.Error
        binding.loadStateErrorMessage.isVisible = loadState is LoadState.Error
    }

    companion object {
        fun create(parent: ViewGroup, retry: () -> Unit): CollectionsLoadStateViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_collections_load_state_footer_view, parent, false)
            val binding = ItemCollectionsLoadStateFooterViewBinding.bind(view)
            return CollectionsLoadStateViewHolder(binding, retry)
        }
    }
}