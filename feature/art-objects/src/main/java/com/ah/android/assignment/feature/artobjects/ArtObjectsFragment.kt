package com.ah.android.assignment.feature.artobjects

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import com.ah.android.assignment.common.exceptionhandling.ExceptionHandling
import com.ah.android.assignment.feature.artobjects.databinding.FragmentArtObjectsBinding
import com.ah.android.assignment.model.collection.Collection
import com.google.android.material.shape.MaterialShapeDrawable
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ArtObjectsFragment : Fragment() {
    private val viewModel by viewModels<ArtObjectsViewModel>()

    private var _binding: FragmentArtObjectsBinding? = null
    private val binding get() = _binding!!
    private val collectionAdapter by lazy { CollectionAdapter(::showArtObjectDetail) }
    private val headerLoadStateAdapter = CollectionsLoadStateAdapter(collectionAdapter::retry)

    @Inject
    lateinit var exceptionHandling: ExceptionHandling

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentArtObjectsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews(view)
        collectUiState()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupViews(view: View) {
        ViewCompat.setOnApplyWindowInsetsListener(view) { insentView, windowInsets ->
            val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
            insentView.updateLayoutParams {
                this as ViewGroup.MarginLayoutParams
                leftMargin = insets.left
                bottomMargin = insets.bottom
                rightMargin = insets.right
                topMargin = insets.top
            }
            WindowInsetsCompat.CONSUMED
        }

        binding.artObjectsCollectionsRecyclerView.adapter = collectionAdapter.withLoadStateHeaderAndFooter(
            header = headerLoadStateAdapter,
            footer = CollectionsLoadStateAdapter(collectionAdapter::retry)
        )
        binding.artObjectsAppBarLayout.statusBarForeground =
            MaterialShapeDrawable.createWithElevationOverlay(context)
    }

    private fun collectUiState() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                viewModel.artObjectsUiState.collectLatest(::renderUiState)
            }
        }
    }

    private fun renderUiState(state: ArtObjectsUiState) {
        when (state) {
            is ArtObjectsUiState.Error -> state.throwable?.let { exception ->
                exceptionHandling.handleException(requireActivity(), exception)
            }

            is ArtObjectsUiState.Success -> showVenues(state.collections)
            else -> {
                // no-op
            }
        }
    }

    private fun showArtObjectDetail(objectNumber: String) {
        val request = NavDeepLinkRequest.Builder
            .fromUri("rijkmuseum://com.ah.android.assignment/fragment_art_object_detail/$objectNumber".toUri())
            .build()
        findNavController().navigate(request)
    }

    private fun showVenues(collections: PagingData<Collection>) {
        viewLifecycleOwner.lifecycleScope.launch {
            collectionAdapter.submitData(collections)
        }
        viewLifecycleOwner.lifecycleScope.launch {
            collectionAdapter.loadStateFlow.collect { loadState ->
                // Show a retry header if there was an error refreshing, and items were previously
                // cached OR default to the default prepend state
                headerLoadStateAdapter.loadState = loadState.source.refresh.takeIf { it is LoadState.Error && collectionAdapter.itemCount > 0 }
                    ?: loadState.prepend

                // Show loading progress bar during initial load or refresh.
                binding.artObjectsProgressBar.isVisible = loadState.source.refresh is LoadState.Loading

                // Toast on any error, regardless of whether it came from RemoteMediator or PagingSource
                val errorState = loadState.source.refresh as? LoadState.Error
                    ?: loadState.source.append as? LoadState.Error
                    ?: loadState.source.prepend as? LoadState.Error

                errorState?.let {
                    exceptionHandling.handleException(requireActivity(), it.error)
                }
            }
        }
    }
}
