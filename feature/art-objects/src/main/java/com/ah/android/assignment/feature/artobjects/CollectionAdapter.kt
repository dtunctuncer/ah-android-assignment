package com.ah.android.assignment.feature.artobjects

import android.annotation.SuppressLint
import android.content.res.Resources.getSystem
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import com.ah.android.assignment.feature.artobjects.databinding.ItemArtObjectBinding
import com.ah.android.assignment.feature.artobjects.databinding.ItemArtObjectHeaderBinding
import com.ah.android.assignment.model.collection.Collection

class CollectionAdapter(private val artObjectListener: (String) -> Unit) : PagingDataAdapter<Collection, ViewHolder>(CollectionComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == R.layout.item_art_object) {
            val binding = ItemArtObjectBinding.inflate(inflater, parent, false)
            ArtObjectViewHolder(binding)
        } else {
            HeaderViewHolder.create(parent, inflater)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (peek(position)) {
            is Collection.ArtObject -> R.layout.item_art_object
            is Collection.Header -> R.layout.item_art_object_header
            else -> throw UnsupportedOperationException("Unknown view")
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val collectionItem = getItem(position)
        collectionItem?.let { collection ->
            when (collection) {
                is Collection.ArtObject -> (holder as ArtObjectViewHolder).bind(collection)
                is Collection.Header -> (holder as HeaderViewHolder).bind(collection)
            }
        }
    }

    inner class ArtObjectViewHolder(private val binding: ItemArtObjectBinding) :
        ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                val item = peek(bindingAdapterPosition) as Collection.ArtObject
                artObjectListener.invoke(item.objectNumber)
            }
        }

        @SuppressLint("SetTextI18n")
        fun bind(item: Collection.ArtObject) {
            binding.itemArtObjectTitle.text = item.title
            binding.itemArtObjectImage.load(
                data = item.imageUrl,
                builder = {
                    size(56.dp)
                    error(com.ah.android.assignment.core.common.R.drawable.ic_broken_image)
                    placeholder(com.ah.android.assignment.core.common.R.drawable.ic_image)
                }
            )
        }

        private val Int.dp: Int get() = (this * getSystem().displayMetrics.density).toInt()
    }

    class HeaderViewHolder(private val binding: ItemArtObjectHeaderBinding) :
        ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(item: Collection.Header) {
            binding.itemArtObjectAuthor.text = item.description
        }

        companion object {
            fun create(parent: ViewGroup, layoutInflater: LayoutInflater): HeaderViewHolder {
                val binding = ItemArtObjectHeaderBinding.inflate(layoutInflater, parent, false)
                return HeaderViewHolder(binding)
            }
        }
    }


    private object CollectionComparator : DiffUtil.ItemCallback<Collection>() {
        override fun areItemsTheSame(oldItem: Collection, newItem: Collection): Boolean {
            return (oldItem is Collection.ArtObject && newItem is Collection.ArtObject && oldItem.title == newItem.title) ||
                    (oldItem is Collection.Header && newItem is Collection.Header && oldItem.description == newItem.description)
        }

        override fun areContentsTheSame(oldItem: Collection, newItem: Collection) = oldItem == newItem
    }
}