package com.ah.android.assignment.feature.artobjects

import androidx.paging.LoadState
import androidx.paging.LoadStates
import androidx.paging.PagingData
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.ah.android.assignment.core.common.R
import com.ah.android.assignment.data.di.DataModule
import com.ah.android.assignment.data.repository.collections.CollectionsRepository
import com.ah.android.assignment.model.collection.Collection
import com.ah.android.assignment.testing.launchFragmentInHiltContainer
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters

@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
@UninstallModules(DataModule::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class ArtObjectFragmentTest {
    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @BindValue
    @JvmField
    val collectionsRepository: CollectionsRepository = mockk()


    private val artObject = Collection.ArtObject(
        objectNumber = "1",
        title = "very nice title",
        author = "Author1",
        imageUrl = "",
    )

    @Before
    fun setUp() {
        every { collectionsRepository.getArtObjects() } returns flowOf(PagingData.from(listOf(artObject)))
        hiltRule.inject()
    }

    @Test
    fun shouldShowArtObject() {
        launchFragmentInHiltContainer<ArtObjectsFragment>()

        artObjects {
            isArtObjectShown(
                name = artObject.title
            )
        }
    }

    @Test
    fun shouldShowArtObjectHeader() {
        launchFragmentInHiltContainer<ArtObjectsFragment>()

        artObjects {
            isHeaderShown(
                author = artObject.author
            )
        }
    }

    @Test
    fun shouldShowErrorMessageWhenThereIsError() {
        val exception = Exception()
        val errorState = LoadState.Error(exception)
        val errorStates = LoadStates(errorState, errorState, errorState)
        every { collectionsRepository.getArtObjects() } returns flowOf(PagingData.empty(errorStates))

        launchFragmentInHiltContainer<ArtObjectsFragment>()

        artObjects {
            isErrorMessageShown(R.string.unknown_error)
        }
    }
}