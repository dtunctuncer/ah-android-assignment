package com.ah.android.assignment.feature.artobjects

import androidx.annotation.StringRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText

fun artObjects(block: ArtObjectRobot.() -> Unit) {
    ArtObjectRobot().apply(block)
}

class ArtObjectRobot {
    fun isErrorMessageShown(@StringRes id: Int) {
        onView(withText(id)).inRoot(isDialog()).check(matches(isDisplayed()))
    }

    fun isArtObjectShown(
        name: String,
    ) {
        onView(withText(name)).check(matches(isDisplayed()))
    }

    fun isHeaderShown(author: String) {
        onView(withText(author)).check(matches(isDisplayed()))
    }

}
