plugins {
    id("com.ah.android.assignment.library.feature")
}

android {
    namespace = "com.ah.android.assignment.feature.artobjects"
    packaging {
        resources.excludes.add("META-INF/*")
    }
}

dependencies {
    implementation(libs.androidx.paging.runtime)
    implementation(libs.androidx.paging.common)
    implementation(libs.androidx.navigation.fragment.ktx)
    implementation(libs.androidx.navigation.ui.ktx)
    testImplementation(libs.androidx.paging.testing)
}
