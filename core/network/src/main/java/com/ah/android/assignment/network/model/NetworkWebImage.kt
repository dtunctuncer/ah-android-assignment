package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkWebImage(
    @SerialName("guid")
    val guid: String,
    @SerialName("height")
    val height: Int,
    @SerialName("offsetPercentageX")
    val offsetPercentageX: Int,
    @SerialName("offsetPercentageY")
    val offsetPercentageY: Int,
    @SerialName("url")
    val url: String,
    @SerialName("width")
    val width: Int
)