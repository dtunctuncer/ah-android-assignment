package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkLabel(
    @SerialName("date")
    val date: String?,
    @SerialName("description")
    val description: String?,
    @SerialName("makerLine")
    val makerLine: String?,
    @SerialName("notes")
    val notes: String?,
    @SerialName("title")
    val title: String?
)