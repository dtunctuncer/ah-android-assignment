package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkAcquisition(
    @SerialName("creditLine")
    val creditLine: String?,
    @SerialName("date")
    val date: String?,
    @SerialName("method")
    val method: String?
)