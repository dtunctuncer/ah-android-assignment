package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkColor(
    @SerialName("hex")
    val hex: String,
    @SerialName("percentage")
    val percentage: Int
)