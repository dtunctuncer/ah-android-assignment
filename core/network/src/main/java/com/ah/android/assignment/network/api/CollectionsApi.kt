package com.ah.android.assignment.network.api

import com.ah.android.assignment.network.model.NetworkCollection
import com.ah.android.assignment.network.model.NetworkCollectionDetail
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CollectionsApi {
    @GET("collection")
    suspend fun getArtObjects(
        @Query("p") page: Int,
        @Query("ps") pageSize: Int,
        @Query("s") sortedBy: String = "artist"
    ): NetworkCollection

    @GET("collection/{objectNumber}")
    suspend fun getArtObjectDetail(@Path("objectNumber") objectNumber: String): NetworkCollectionDetail
}
