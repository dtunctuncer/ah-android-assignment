package com.ah.android.assignment.network

import com.ah.android.assignment.network.model.NetworkCollection
import com.ah.android.assignment.network.model.NetworkCollectionDetail

interface CollectionsOnlineDataSource {
    suspend fun getArtObjects(page: Int, pageSize: Int): NetworkCollection
    suspend fun getArtObjectDetail(objectNumber: String): NetworkCollectionDetail
}
