package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkNormalized32Color(
    @SerialName("hex")
    val hex: String,
    @SerialName("percentage")
    val percentage: Int
)