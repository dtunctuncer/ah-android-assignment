package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkDating(
    @SerialName("period")
    val period: Int,
    @SerialName("presentingDate")
    val presentingDate: String,
    @SerialName("sortingDate")
    val sortingDate: Int,
    @SerialName("yearEarly")
    val yearEarly: Int,
    @SerialName("yearLate")
    val yearLate: Int
)