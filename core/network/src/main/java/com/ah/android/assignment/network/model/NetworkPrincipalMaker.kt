package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkPrincipalMaker(
    @SerialName("biography")
    val biography: String?,
    @SerialName("dateOfBirth")
    val dateOfBirth: String?,
    @SerialName("dateOfBirthPrecision")
    val dateOfBirthPrecision: String?,
    @SerialName("dateOfDeath")
    val dateOfDeath: String?,
    @SerialName("dateOfDeathPrecision")
    val dateOfDeathPrecision: String?,
    @SerialName("name")
    val name: String?,
    @SerialName("nationality")
    val nationality: String?,
    @SerialName("occupation")
    val occupation: List<String>,
    @SerialName("placeOfBirth")
    val placeOfBirth: String?,
    @SerialName("placeOfDeath")
    val placeOfDeath: String?,
    @SerialName("productionPlaces")
    val productionPlaces: List<String>,
    @SerialName("qualification")
    val qualification: String?,
    @SerialName("roles")
    val roles: List<String>,
    @SerialName("unFixedName")
    val unFixedName: String
)