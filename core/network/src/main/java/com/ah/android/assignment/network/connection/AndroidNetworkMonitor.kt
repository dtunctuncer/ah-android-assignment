package com.ah.android.assignment.network.connection

import android.net.ConnectivityManager
import javax.inject.Inject

class AndroidNetworkMonitor @Inject constructor(
    private val connectivityManager: ConnectivityManager
) : NetworkMonitor {

    override val isNetworkAvailable: Boolean
        get() {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }
}