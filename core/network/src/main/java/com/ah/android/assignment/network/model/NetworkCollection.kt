package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkCollection(
    @SerialName("artObjects")
    val artObjects: List<NetworkArtObject>,
    @SerialName("count")
    val count: Int,
    @SerialName("countFacets")
    val countFacets: NetworkCountFacets?
)