package com.ah.android.assignment.network.di

import android.content.Context
import android.net.ConnectivityManager
import androidx.paging.PagingSource
import com.ah.android.assignment.model.collection.Collection
import com.ah.android.assignment.network.BuildConfig
import com.ah.android.assignment.network.CollectionsOnlineDataSource
import com.ah.android.assignment.network.api.CollectionsApi
import com.ah.android.assignment.network.connection.AndroidNetworkMonitor
import com.ah.android.assignment.network.connection.ErrorCallAdapterFactory
import com.ah.android.assignment.network.connection.NetworkMonitor
import com.ah.android.assignment.network.datasource.CollectionsOnlineDataSourceImpl
import com.ah.android.assignment.network.datasource.CollectionsOnlinePagingDataSource
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface NetworkModule {

    @Binds
    fun bindCollectionsOnlineDataSource(
        collectionsOnlineDataSourceImpl: CollectionsOnlineDataSourceImpl
    ): CollectionsOnlineDataSource

    @Binds
    fun bindCollectionsOnlinePagingDataSource(
        collectionsOnlinePagingDataSource: CollectionsOnlinePagingDataSource
    ): PagingSource<Int, Collection>

    companion object {

        @Provides
        @Singleton
        fun provideJson(): Json = Json {
            ignoreUnknownKeys = true
            explicitNulls = false
        }

        @Provides
        @Singleton
        fun provideNetworkMonitor(
            @ApplicationContext context: Context
        ): NetworkMonitor {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return AndroidNetworkMonitor(connectivityManager)
        }

        @Provides
        @Singleton
        fun provideOkHttpClient() = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                setLevel(HttpLoggingInterceptor.Level.BODY)
            })
            .addInterceptor { chain ->
                val originalUrl = chain.request().url
                val newRequest = chain.request().newBuilder()
                val newUrl = originalUrl.newBuilder().addQueryParameter("key", BuildConfig.API_KEY).build()
                newRequest.url(newUrl)
                chain.proceed(newRequest.build())
            }
            .build()

        @Provides
        @Singleton
        fun provideCollectionsApi(networkJson: Json, okHttpClient: OkHttpClient, networkMonitor: NetworkMonitor): CollectionsApi = Retrofit.Builder()
            .baseUrl(BuildConfig.FOURSQUARE_BASE_URL)
            .client(okHttpClient)
            .addCallAdapterFactory(ErrorCallAdapterFactory(networkMonitor))
            .addConverterFactory(
                @OptIn(ExperimentalSerializationApi::class)
                networkJson.asConverterFactory("application/json".toMediaType())
            )
            .build()
            .create(CollectionsApi::class.java)

    }
}
