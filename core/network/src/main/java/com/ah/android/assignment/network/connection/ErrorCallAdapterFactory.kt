package com.ah.android.assignment.network.connection

import com.ah.android.assignment.common.exceptionhandling.exception.NoInternetConnectionException
import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

private class ErrorCall<R>(
    private val delegate: Call<R>,
    private val successType: Type,
    private val networkMonitor: NetworkMonitor
) : Call<R> {

    override fun enqueue(callback: Callback<R>) {
        if (!networkMonitor.isNetworkAvailable) {
            callback.onFailure(delegate, NoInternetConnectionException())
        } else {
            delegate.enqueue(
                object : Callback<R> {
                    override fun onResponse(call: Call<R>, response: Response<R>) {
                        callback.onResponse(this@ErrorCall, response)
                    }

                    override fun onFailure(call: Call<R>, throwable: Throwable) {
                        callback.onFailure(this@ErrorCall, throwable)
                    }
                }
            )
        }
    }

    override fun clone(): Call<R> {
        return delegate.clone()
    }

    override fun execute(): Response<R> {
        return delegate.execute()
    }

    override fun isExecuted(): Boolean {
        return delegate.isExecuted
    }

    override fun cancel() {
        return delegate.cancel()
    }

    override fun isCanceled(): Boolean {
        return delegate.isCanceled
    }

    override fun request(): Request {
        return delegate.request()
    }

    override fun timeout(): Timeout {
        return delegate.timeout()
    }
}

private class ErrorCallAdapter<R>(
    private val successType: Type,
    private val networkMonitor: NetworkMonitor
) : CallAdapter<R, Call<R>> {

    override fun adapt(call: Call<R>): Call<R> = ErrorCall(call, successType, networkMonitor)

    override fun responseType(): Type = successType
}

internal class ErrorCallAdapterFactory(private val networkMonitor: NetworkMonitor) : CallAdapter.Factory() {
    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *> {
        val enclosingType = (returnType as ParameterizedType)
        val type = enclosingType.actualTypeArguments[0]
        return ErrorCallAdapter<Any>(type, networkMonitor)
    }
}