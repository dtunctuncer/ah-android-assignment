package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkDimension(
    @SerialName("part")
    val part: String?,
    @SerialName("type")
    val type: String?,
    @SerialName("unit")
    val unit: String?,
    @SerialName("value")
    val value: String?
)