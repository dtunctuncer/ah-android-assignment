package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkArtObjectDetail(
    @SerialName("acquisition")
    val acquisition: NetworkAcquisition,
    @SerialName("artistRole")
    val artistRole: String?,
    @SerialName("associations")
    val associations: List<String>,
    @SerialName("catRefRPK")
    val catRefRPK: List<String>,
    @SerialName("classification")
    val classification: NetworkClassification,
    @SerialName("colors")
    val colors: List<NetworkColor>,
    @SerialName("colorsWithNormalization")
    val colorsWithNormalization: List<NetworkColorsWithNormalization>,
    @SerialName("copyrightHolder")
    val copyrightHolder: String?,
    @SerialName("dating")
    val dating: NetworkDating,
    @SerialName("description")
    val description: String?,
    @SerialName("dimensions")
    val dimensions: List<NetworkDimension>,
    @SerialName("documentation")
    val documentation: List<String>,
    @SerialName("exhibitions")
    val exhibitions: List<String>,
    @SerialName("hasImage")
    val hasImage: Boolean,
    @SerialName("historicalPersons")
    val historicalPersons: List<String>,
    @SerialName("id")
    val id: String,
    @SerialName("inscriptions")
    val inscriptions: List<String>,
    @SerialName("label")
    val label: NetworkLabel,
    @SerialName("labelText")
    val labelText: String?,
    @SerialName("language")
    val language: String,
    @SerialName("location")
    val location: String?,
    @SerialName("longTitle")
    val longTitle: String,
    @SerialName("makers")
    val makers: List<NetworkPrincipalMaker>,
    @SerialName("materials")
    val materials: List<String>,
    @SerialName("normalized32Colors")
    val normalized32Colors: List<NetworkNormalized32Color>,
    @SerialName("normalizedColors")
    val normalizedColors: List<NetworkNormalized32Color>,
    @SerialName("objectCollection")
    val objectCollection: List<String>,
    @SerialName("objectNumber")
    val objectNumber: String,
    @SerialName("objectTypes")
    val objectTypes: List<String>,
    @SerialName("physicalMedium")
    val physicalMedium: String,
    @SerialName("physicalProperties")
    val physicalProperties: List<String>,
    @SerialName("plaqueDescriptionDutch")
    val plaqueDescriptionDutch: String?,
    @SerialName("plaqueDescriptionEnglish")
    val plaqueDescriptionEnglish: String?,
    @SerialName("principalMaker")
    val principalMaker: String,
    @SerialName("principalMakers")
    val principalMakers: List<NetworkPrincipalMaker>,
    @SerialName("principalOrFirstMaker")
    val principalOrFirstMaker: String?,
    @SerialName("priref")
    val priref: String?,
    @SerialName("productionPlaces")
    val productionPlaces: List<String>,
    @SerialName("scLabelLine")
    val scLabelLine: String,
    @SerialName("showImage")
    val showImage: Boolean,
    @SerialName("subTitle")
    val subTitle: String,
    @SerialName("techniques")
    val techniques: List<String>,
    @SerialName("title")
    val title: String,
    @SerialName("titles")
    val titles: List<String>,
    @SerialName("webImage")
    val webImage: NetworkWebImage? = null
)