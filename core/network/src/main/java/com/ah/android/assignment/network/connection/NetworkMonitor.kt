package com.ah.android.assignment.network.connection

interface NetworkMonitor {
    val isNetworkAvailable: Boolean
}