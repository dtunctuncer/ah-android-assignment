package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkColorsWithNormalization(
    @SerialName("normalizedHex")
    val normalizedHex: String,
    @SerialName("originalHex")
    val originalHex: String
)