package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkArtObject(
    @SerialName("hasImage")
    val hasImage: Boolean,
    @SerialName("headerImage")
    val headerImage: NetworkHeaderImage,
    @SerialName("id")
    val id: String,
    @SerialName("longTitle")
    val longTitle: String,
    @SerialName("objectNumber")
    val objectNumber: String,
    @SerialName("permitDownload")
    val permitDownload: Boolean,
    @SerialName("principalOrFirstMaker")
    val principalOrFirstMaker: String,
    @SerialName("productionPlaces")
    val productionPlaces: List<String>,
    @SerialName("showImage")
    val showImage: Boolean,
    @SerialName("title")
    val title: String,
    @SerialName("webImage")
    val webImage: NetworkWebImage? = null
)