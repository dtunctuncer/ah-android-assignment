package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkClassification(
    @SerialName("iconClassIdentifier")
    val iconClassIdentifier: List<String>
)