package com.ah.android.assignment.network.datasource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.ah.android.assignment.common.exceptionhandling.exception.NoInternetConnectionException
import com.ah.android.assignment.model.collection.Collection
import com.ah.android.assignment.network.CollectionsOnlineDataSource
import javax.inject.Inject

class CollectionsOnlinePagingDataSource @Inject constructor(
    private val collectionsOnlineDataSource: CollectionsOnlineDataSource
) : PagingSource<Int, Collection>() {
    override fun getRefreshKey(state: PagingState<Int, Collection>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Collection> {
        return try {
            val page = params.key ?: STARTING_PAGE_INDEX
            val response = collectionsOnlineDataSource.getArtObjects(page = page, pageSize = params.loadSize)
            LoadResult.Page(
                data = response.artObjects.map {
                    Collection.ArtObject(it.objectNumber, it.title, it.principalOrFirstMaker, it.webImage?.url)
                },
                prevKey = if (page == STARTING_PAGE_INDEX) null else page.minus(1),
                nextKey = if (response.artObjects.isEmpty()) null else page.plus(1)
            )
        } catch (exception: NoInternetConnectionException) {
            LoadResult.Error(exception)
        }
    }

    companion object {
        private const val STARTING_PAGE_INDEX = 0
    }

}
