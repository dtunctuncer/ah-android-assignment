package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkCollectionDetail(
    @SerialName("artObject")
    val artObject: NetworkArtObjectDetail
)