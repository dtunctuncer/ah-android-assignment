package com.ah.android.assignment.network.datasource

import com.ah.android.assignment.network.CollectionsOnlineDataSource
import com.ah.android.assignment.network.api.CollectionsApi
import com.ah.android.assignment.network.model.NetworkCollection
import com.ah.android.assignment.network.model.NetworkCollectionDetail
import javax.inject.Inject

class CollectionsOnlineDataSourceImpl @Inject constructor(
    private val collectionsApi: CollectionsApi
) : CollectionsOnlineDataSource {
    override suspend fun getArtObjects(page: Int, pageSize: Int): NetworkCollection {
        return collectionsApi.getArtObjects(page, pageSize)
    }

    override suspend fun getArtObjectDetail(objectNumber: String): NetworkCollectionDetail {
        return collectionsApi.getArtObjectDetail(objectNumber)
    }
}
