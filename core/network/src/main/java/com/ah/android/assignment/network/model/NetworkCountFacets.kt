package com.ah.android.assignment.network.model


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class NetworkCountFacets(
    @SerialName("hasimage")
    val hasImage: Int,
    @SerialName("ondisplay")
    val onDisplay: Int
)