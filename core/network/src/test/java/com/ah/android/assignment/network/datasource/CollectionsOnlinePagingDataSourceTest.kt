package com.ah.android.assignment.network.datasource

import androidx.paging.PagingConfig
import androidx.paging.PagingSource
import androidx.paging.testing.TestPager
import com.ah.android.assignment.network.networkArtObjects
import kotlinx.coroutines.test.runTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.instanceOf
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CollectionsOnlinePagingDataSourceTest {
    private lateinit var sut: CollectionsOnlinePagingDataSource
    private lateinit var fakeCollectionOnlineDataSource: FakeCollectionOnlineDataSource

    @Before
    fun setUp() {
        fakeCollectionOnlineDataSource = FakeCollectionOnlineDataSource()
        sut = CollectionsOnlinePagingDataSource(fakeCollectionOnlineDataSource)
    }

    @Test
    fun `should load returns page when item keyed data is successful`() = runTest {
        val pager = TestPager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSource = sut
        )

        val result = pager.refresh()

        assertThat(result, instanceOf(PagingSource.LoadResult.Page::class.java))
    }

    @Test
    fun `should load returns error when there is no internet connection`() = runTest {
        fakeCollectionOnlineDataSource.shouldThrowInternetNoInternetError = true
        val pager = TestPager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSource = sut
        )

        val result = pager.refresh()

        assertThat(result, instanceOf(PagingSource.LoadResult.Error::class.java))
    }

    @Test
    fun `should throw random error when error is not no internet connection`() = runTest {
        fakeCollectionOnlineDataSource.shouldThrowRandomError = true
        val pager = TestPager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSource = sut
        )

        val exception = runCatching { pager.refresh() }.exceptionOrNull()

        assertThat(exception, instanceOf(IllegalAccessException::class.java))
    }

    @Test
    fun `should fetch the items when load is successful`() = runTest {
        val pager = TestPager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSource = sut
        )

        val result = pager.refresh() as PagingSource.LoadResult.Page

        assertEquals(result.data.count(), networkArtObjects.count())
    }
}