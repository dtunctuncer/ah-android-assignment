package com.ah.android.assignment.network

import com.ah.android.assignment.network.model.NetworkArtObject
import com.ah.android.assignment.network.model.NetworkHeaderImage
import com.ah.android.assignment.network.model.NetworkWebImage

val networkArtObject = NetworkArtObject(
    hasImage = true,
    headerImage = NetworkHeaderImage(
        guid = "e8a906c8-cba2-4a70-ac31-1d2533dcfe29",
        offsetPercentageX = 50,
        offsetPercentageY = 50,
        width = 2500,
        height = 1307,
        url = "https://lh3.googleusercontent.com/3lQgqHjtqBjoiqy8QfMrPRCYjE8Pg8Y2g-MEn4lwG4MN-pYdGs9gvJ1FhGfXKYZmGB6d92NIg3KJZldcLP52S5pOPKE=s0"
    ),
    id = "nl-NG-MC-1350-F",
    longTitle = "Model van een boei, 's Rijks Algemeen Betonningsmagazijn, 1869 - 1873",
    objectNumber = "NG-MC-1350-F",
    permitDownload = true,
    principalOrFirstMaker = "'s Rijks Algemeen Betonningsmagazijn",
    productionPlaces = emptyList(),
    showImage = true,
    title = "Model van een boei",
    webImage = NetworkWebImage(
        guid = "e8a906c8-cba2-4a70-ac31-1d2533dcfe29",
        offsetPercentageX = 50,
        offsetPercentageY = 50,
        width = 2500,
        height = 1307,
        url = "https://lh3.googleusercontent.com/3lQgqHjtqBjoiqy8QfMrPRCYjE8Pg8Y2g-MEn4lwG4MN-pYdGs9gvJ1FhGfXKYZmGB6d92NIg3KJZldcLP52S5pOPKE=s0"
    )
)

val networkArtObjects = listOf(
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
    networkArtObject,
)
