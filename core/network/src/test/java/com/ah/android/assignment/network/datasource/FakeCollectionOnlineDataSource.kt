package com.ah.android.assignment.network.datasource

import com.ah.android.assignment.common.exceptionhandling.exception.NoInternetConnectionException
import com.ah.android.assignment.network.CollectionsOnlineDataSource
import com.ah.android.assignment.network.model.NetworkCollection
import com.ah.android.assignment.network.model.NetworkCollectionDetail
import com.ah.android.assignment.network.networkArtObjects

class FakeCollectionOnlineDataSource : CollectionsOnlineDataSource {
    var shouldThrowInternetNoInternetError = false
    var shouldThrowRandomError = false
    override suspend fun getArtObjects(page: Int, pageSize: Int): NetworkCollection {
        if (shouldThrowInternetNoInternetError) {
            throw NoInternetConnectionException()
        } else if (shouldThrowRandomError) {
            throw IllegalAccessException("Random Error")
        } else {
            return NetworkCollection(networkArtObjects, networkArtObjects.count(), null)
        }
    }

    override suspend fun getArtObjectDetail(objectNumber: String): NetworkCollectionDetail {
        throw IllegalAccessException("getArtObjectDetail is not implemented")
    }
}