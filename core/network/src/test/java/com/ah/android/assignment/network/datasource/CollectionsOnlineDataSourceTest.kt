package com.ah.android.assignment.network.datasource

import com.ah.android.assignment.network.CollectionsOnlineDataSource
import com.ah.android.assignment.network.api.CollectionsApi
import com.ah.android.assignment.network.model.NetworkCollection
import com.ah.android.assignment.network.model.NetworkCollectionDetail
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CollectionsOnlineDataSourceTest {
    private val testDispatcher = StandardTestDispatcher()

    private lateinit var collectionsApi: CollectionsApi
    private lateinit var sut: CollectionsOnlineDataSource

    @Before
    fun setUp() {
        collectionsApi = mockk()
        sut = CollectionsOnlineDataSourceImpl(collectionsApi)
    }

    @Test
    fun `getArtObjects returns the response without changing`() = runTest(testDispatcher) {
        val networkCollection = NetworkCollection(emptyList(), 0, mockk())
        coEvery { collectionsApi.getArtObjects(any(), any()) } returns networkCollection

        val result = sut.getArtObjects(0, 0)

        coVerify { collectionsApi.getArtObjects(eq(0), eq(0)) }
        assertEquals(result, networkCollection)
    }

    @Test
    fun `getArtObjects throws exception`() = runTest(testDispatcher) {
        val exception = Exception("test")
        coEvery { collectionsApi.getArtObjects(any(), any()) } throws exception

        val resultException = runCatching { sut.getArtObjects(0, 0) }.exceptionOrNull()

        coVerify { collectionsApi.getArtObjects(eq(0), eq(0)) }
        assertEquals(exception, resultException)
    }

    @Test
    fun `getArtObjectDetail returns the response without changing`() = runTest(testDispatcher) {
        val networkCollectionDetail = NetworkCollectionDetail(mockk())
        coEvery { collectionsApi.getArtObjectDetail(any()) } returns networkCollectionDetail

        val result = sut.getArtObjectDetail("")

        coVerify { collectionsApi.getArtObjectDetail(eq("")) }
        assertEquals(result, networkCollectionDetail)
    }

    @Test
    fun `getArtObjectDetail throws exception`() = runTest(testDispatcher) {
        val exception = Exception("test")
        coEvery { collectionsApi.getArtObjectDetail(any()) } throws exception

        val resultException = runCatching { sut.getArtObjectDetail("") }.exceptionOrNull()

        coVerify { collectionsApi.getArtObjectDetail(eq("")) }
        assertEquals(exception, resultException)
    }
}
