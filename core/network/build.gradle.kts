plugins {
    id("com.ah.android.assignment.library")
    id("com.ah.android.assignment.hilt")
    id("kotlinx-serialization")
}

apply(from = "local.gradle")

android {
    namespace = "com.ah.android.assignment.network"
    defaultConfig {
        buildConfigField("String", "FOURSQUARE_BASE_URL", "\"https://www.rijksmuseum.nl/api/nl/\"")
    }
}

dependencies {
    implementation(project(":core:common"))
    implementation(project(":core:model"))

    implementation(libs.kotlinx.coroutines.android)
    implementation(libs.kotlinx.serialization.json)

    implementation(libs.okhttp.logging)
    implementation(libs.retrofit.core)
    implementation(libs.retrofit.kotlin.serialization)
    implementation(libs.androidx.paging.runtime)

    testImplementation(project(":core:testing"))
    testImplementation(libs.androidx.paging.testing)
}
