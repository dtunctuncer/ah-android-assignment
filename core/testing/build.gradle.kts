plugins {
    id("com.ah.android.assignment.library")
    id("com.ah.android.assignment.hilt")
}

android {
    namespace = "com.ah.android.assignment.core.testing"
}

dependencies {
    implementation(project(":core:common"))
    implementation(project(":core:data"))
    implementation(project(":core:model"))
    debugImplementation(libs.androidx.appcompat)
    debugImplementation(libs.material)

    api(libs.junit4)
    api(libs.androidx.test.core)
    api(libs.kotlinx.coroutines.test)
    api(libs.turbine)

    api(libs.mockk.android)
    api(libs.mockk.agent)

    api(libs.androidx.test.espresso.core)
    api(libs.androidx.test.runner)
    api(libs.androidx.test.rules)
    api(libs.hilt.android.testing)
    api(libs.androidx.test.ext)
    api(libs.androidx.lifecycle.runtime.ktx)
    api(libs.androidx.test.uiautomator)
    api(libs.androidx.paging.common)
    debugApi(libs.androidx.fragment.testing)
}
