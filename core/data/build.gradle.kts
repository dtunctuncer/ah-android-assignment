plugins {
    id("com.ah.android.assignment.library")
    id("com.ah.android.assignment.hilt")
    id("kotlinx-serialization")
}

android {
    namespace = "com.ah.android.assignment.data"
    testOptions {
        unitTests {
            isIncludeAndroidResources = true
        }
    }
}

dependencies {
    implementation(project(":core:common"))
    implementation(project(":core:model"))
    implementation(project(":core:network"))
    implementation(libs.androidx.paging.runtime)

    implementation(libs.androidx.core.ktx)

    implementation(libs.kotlinx.coroutines.android)
    implementation(libs.kotlinx.serialization.json)

    testImplementation(project(":core:testing"))
    testImplementation("junit:junit:4.13.2")
}
