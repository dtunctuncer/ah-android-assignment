package com.ah.android.assignment.data.di

import com.ah.android.assignment.data.repository.collections.CollectionsRepository
import com.ah.android.assignment.data.repository.collections.CollectionsRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface DataModule {

    @Binds
    fun bindsCollectionsRepository(
        collectionsRepositoryImpl: CollectionsRepositoryImpl
    ): CollectionsRepository
}
