package com.ah.android.assignment.data.repository.collections

import androidx.paging.PagingData
import com.ah.android.assignment.model.collection.ArtObjectDetail
import com.ah.android.assignment.model.collection.Collection
import kotlinx.coroutines.flow.Flow

interface CollectionsRepository {
    fun getArtObjects(): Flow<PagingData<Collection>>
    fun getArtObjectDetail(objectNumber: String): Flow<ArtObjectDetail>
}
