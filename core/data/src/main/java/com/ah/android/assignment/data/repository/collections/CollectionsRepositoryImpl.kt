package com.ah.android.assignment.data.repository.collections

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.PagingSource
import com.ah.android.assignment.model.collection.ArtObjectDetail
import com.ah.android.assignment.model.collection.Collection
import com.ah.android.assignment.network.CollectionsOnlineDataSource
import com.ah.android.assignment.network.model.NetworkArtObjectDetail
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class CollectionsRepositoryImpl @Inject constructor(
    private val collectionsOnlineDataSource: CollectionsOnlineDataSource,
    private val collectionsOnlinePagingDataSource: PagingSource<Int, Collection>
) : CollectionsRepository {
    override fun getArtObjects(): Flow<PagingData<Collection>> = Pager(
        config = PagingConfig(
            pageSize = 10,
            enablePlaceholders = false
        ),
        initialKey = 0,
        pagingSourceFactory = { collectionsOnlinePagingDataSource }
    ).flow

    override fun getArtObjectDetail(objectNumber: String): Flow<ArtObjectDetail> = flow {
        emit(collectionsOnlineDataSource.getArtObjectDetail(objectNumber).artObject.asExternalModel())
    }
}

fun NetworkArtObjectDetail.asExternalModel(): ArtObjectDetail {
    return ArtObjectDetail(
        objectNumber = objectNumber,
        title = title,
        longTitle = longTitle,
        subtitle = subTitle,
        imageUrl = webImage?.url,
        description = description,
        objectCollection = objectCollection.joinToString(),
        objectTypes = objectTypes.joinToString(),
        maker = principalMaker,
        materials = materials.joinToString()
    )
}
