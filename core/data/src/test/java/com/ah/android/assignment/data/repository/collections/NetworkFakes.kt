package com.ah.android.assignment.data.repository.collections

import com.ah.android.assignment.network.model.NetworkAcquisition
import com.ah.android.assignment.network.model.NetworkArtObject
import com.ah.android.assignment.network.model.NetworkArtObjectDetail
import com.ah.android.assignment.network.model.NetworkClassification
import com.ah.android.assignment.network.model.NetworkCollectionDetail
import com.ah.android.assignment.network.model.NetworkDating
import com.ah.android.assignment.network.model.NetworkDimension
import com.ah.android.assignment.network.model.NetworkHeaderImage
import com.ah.android.assignment.network.model.NetworkLabel
import com.ah.android.assignment.network.model.NetworkPrincipalMaker
import com.ah.android.assignment.network.model.NetworkWebImage


val networkArtObjectDetail = NetworkArtObjectDetail(
    acquisition = NetworkAcquisition(
        creditLine = null,
        date = "1883-01-01T00:00:00",
        method = "overdracht van beheer"
    ),
    artistRole = null,
    associations = emptyList(),
    catRefRPK = emptyList(),
    classification = NetworkClassification(
        iconClassIdentifier = emptyList()
    ),
    colors = emptyList(),
    colorsWithNormalization = emptyList(),
    copyrightHolder = null,
    dating = NetworkDating(
        period = 19,
        presentingDate = "1869 - 1873",
        sortingDate = 1869,
        yearEarly = 1869,
        yearLate = 1873
    ),
    description = "Model van een boei (buikton) met een conisch lichaam met een scherp uiteinde en een ronde platte bovenkant met een ring in het midden. Zoals een vat is zij samengesteld uit duigen en hoepels. Onderaan zit een oog voor de ankerketting. Gemerkt met gekruiste ankers. Model behoort bij serie van zes boeien van verschillende formaten. Schaal 1:10 (archief).",
    dimensions = listOf(
        NetworkDimension(
            unit = "cm",
            type = "lengte",
            part = null,
            value = "17,4"
        ),
        NetworkDimension(
            unit = "cm",
            type = "diameter",
            part = null,
            value = "9,4"
        )
    ),
    documentation = emptyList(),
    exhibitions = emptyList(),
    hasImage = true,
    historicalPersons = emptyList(),
    id = "nl-NG-MC-1350-F",
    inscriptions = emptyList(),
    label = NetworkLabel(
        title = null,
        makerLine = null,
        description = null,
        notes = null,
        date = null
    ),
    labelText = null,
    language = "nl",
    longTitle = "Model van een boei, 's Rijks Algemeen Betonningsmagazijn, 1869 - 1873",
    location = null,
    makers = emptyList(),
    materials = listOf("hout", "ijzer"),
    normalizedColors = emptyList(),
    normalized32Colors = emptyList(),
    objectNumber = "NG-MC-1350-F",
    objectCollection = listOf("Marinemodellenkamer"),
    objectTypes = listOf("demonstratiemodel"),
    physicalMedium = "hout en ijzer",
    physicalProperties = emptyList(),
    plaqueDescriptionDutch = null,
    plaqueDescriptionEnglish = null,
    principalMaker = "'s Rijks Algemeen Betonningsmagazijn",
    principalMakers = listOf(
        NetworkPrincipalMaker(
            name = "s Rijks Algemeen Betonningsmagazijn",
            unFixedName = "'s Rijks Algemeen Betonningsmagazijn",
            placeOfBirth = null,
            dateOfBirth = null,
            dateOfBirthPrecision = null,
            dateOfDeath = null,
            dateOfDeathPrecision = null,
            placeOfDeath = null,
            occupation = emptyList(),
            roles = listOf("modelmaker"),
            nationality = null,
            biography = null,
            productionPlaces = listOf("Enkhuizen"),
            qualification = null
        )
    ),
    principalOrFirstMaker = "'s Rijks Algemeen Betonningsmagazijn",
    priref = "649653",
    productionPlaces = emptyList(),
    scLabelLine = "'s Rijks Algemeen Betonningsmagazijn, 1869 - 1873, hout en ijzer",
    showImage = true,
    subTitle = "l 17,4cm × d 9,4cm",
    techniques = emptyList(),
    title = "Model van een boei",
    titles = listOf("Model van een boei"),
    webImage = NetworkWebImage(
        guid = "e8a906c8-cba2-4a70-ac31-1d2533dcfe29",
        offsetPercentageX = 50,
        offsetPercentageY = 50,
        width = 2500,
        height = 1307,
        url = "https://lh3.googleusercontent.com/3lQgqHjtqBjoiqy8QfMrPRCYjE8Pg8Y2g-MEn4lwG4MN-pYdGs9gvJ1FhGfXKYZmGB6d92NIg3KJZldcLP52S5pOPKE=s0"
    )
)

val networkCollectionDetail = NetworkCollectionDetail(networkArtObjectDetail)


val networkArtObject = NetworkArtObject(
    hasImage = true,
    headerImage = NetworkHeaderImage(
        guid = "e8a906c8-cba2-4a70-ac31-1d2533dcfe29",
        offsetPercentageX = 50,
        offsetPercentageY = 50,
        width = 2500,
        height = 1307,
        url = "https://lh3.googleusercontent.com/3lQgqHjtqBjoiqy8QfMrPRCYjE8Pg8Y2g-MEn4lwG4MN-pYdGs9gvJ1FhGfXKYZmGB6d92NIg3KJZldcLP52S5pOPKE=s0"
    ),
    id = "nl-NG-MC-1350-F",
    longTitle = "Model van een boei, 's Rijks Algemeen Betonningsmagazijn, 1869 - 1873",
    objectNumber = "NG-MC-1350-F",
    permitDownload = true,
    principalOrFirstMaker = "'s Rijks Algemeen Betonningsmagazijn",
    productionPlaces = emptyList(),
    showImage = true,
    title = "Model van een boei",
    webImage = NetworkWebImage(
        guid = "e8a906c8-cba2-4a70-ac31-1d2533dcfe29",
        offsetPercentageX = 50,
        offsetPercentageY = 50,
        width = 2500,
        height = 1307,
        url = "https://lh3.googleusercontent.com/3lQgqHjtqBjoiqy8QfMrPRCYjE8Pg8Y2g-MEn4lwG4MN-pYdGs9gvJ1FhGfXKYZmGB6d92NIg3KJZldcLP52S5pOPKE=s0"
    )
)
