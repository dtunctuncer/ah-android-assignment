package com.ah.android.assignment.data.repository.collections

import com.ah.android.assignment.network.CollectionsOnlineDataSource
import com.ah.android.assignment.network.datasource.CollectionsOnlinePagingDataSource
import com.ah.android.assignment.network.model.NetworkCollection
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.instanceOf
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CollectionsRepositoryTest {
    private val testDispatcher = UnconfinedTestDispatcher()
    private lateinit var collectionsOnlineDataSource: CollectionsOnlineDataSource
    private lateinit var collectionsOnlinePagingDataSource: CollectionsOnlinePagingDataSource
    private lateinit var sut: CollectionsRepository


    @Before
    fun setUp() {
        collectionsOnlineDataSource = mockk()
        collectionsOnlinePagingDataSource = CollectionsOnlinePagingDataSource(collectionsOnlineDataSource)
        sut = CollectionsRepositoryImpl(collectionsOnlineDataSource, collectionsOnlinePagingDataSource)

    }

    @Test
    fun `getArtObjectDetail should backed by online data source`() = runTest(testDispatcher) {
        coEvery { collectionsOnlineDataSource.getArtObjectDetail(any()) } returns networkCollectionDetail

        val artObjectDetail = sut.getArtObjectDetail("nl-NG-MC-1350-F").first()

        assertEquals(artObjectDetail, networkCollectionDetail.artObject.asExternalModel())
    }

    @Test
    fun `getArtObjects should backed by online paging data source`() = runTest(testDispatcher) {
        coEvery { collectionsOnlineDataSource.getArtObjects(any(), any()) } returns NetworkCollection(
            artObjects = listOf(
                networkArtObject,
                networkArtObject,
                networkArtObject,
                networkArtObject,
                networkArtObject,
                networkArtObject,
                networkArtObject
            ),
            count = 8,
            countFacets = null
        )

        val artObjectFlow = sut.getArtObjects()

        assertThat(artObjectFlow, instanceOf(Flow::class.java))
    }
}
