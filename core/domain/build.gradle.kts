plugins {
    id("com.ah.android.assignment.library")
    kotlin("kapt")
}

android {
    namespace = "com.ah.android.assignment.domain"
}

dependencies {

    implementation(project(":core:data"))
    implementation(project(":core:model"))

    testImplementation(project(":core:testing"))
    testImplementation(libs.androidx.paging.testing)

    implementation(libs.androidx.paging.runtime)

    implementation(libs.kotlinx.coroutines.android)
    implementation(libs.kotlinx.datetime)

    implementation(libs.hilt.android)
    kapt(libs.hilt.compiler)
}
