package com.ah.android.domain

import androidx.paging.insertSeparators
import com.ah.android.assignment.data.repository.collections.CollectionsRepository
import com.ah.android.assignment.model.collection.Collection
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GetCollectionsUseCase @Inject constructor(
    private val collectionsRepository: CollectionsRepository
) {
    operator fun invoke() =
        collectionsRepository.getArtObjects()
            .map { pagingData ->
                pagingData.insertSeparators { before, after ->
                    val beforeArtObject = before as? Collection.ArtObject
                    val afterArtObject = after as? Collection.ArtObject
                    when {
                        // we're at the beginning of the list
                        before == null -> Collection.Header(afterArtObject?.author ?: "")
                        // we're at the end of the list
                        after == null -> null
                        beforeArtObject?.author != afterArtObject?.author -> Collection.Header(afterArtObject?.author ?: "")
                        // Return null to avoid adding a separator between two items.
                        else -> null
                    }
                }
            }
}
