package com.ah.android.domain

import androidx.paging.testing.asSnapshot
import com.ah.android.assignment.model.collection.Collection
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class GetCollectionsUseCaseTest {
    private val testDispatcher = UnconfinedTestDispatcher()
    private lateinit var getCollectionsUseCase: GetCollectionsUseCase
    private lateinit var collectionsRepository: FakeCollectionsRepository

    @Before
    fun setUp() {
        collectionsRepository = FakeCollectionsRepository()
        getCollectionsUseCase = GetCollectionsUseCase(collectionsRepository)
    }

    @Test
    fun `first item always should be header`() = runTest(testDispatcher) {
        collectionsRepository.pushArtObjects(createArtObjectsWithAuthor("author1"))

        val items = getCollectionsUseCase.invoke()

        val snapshotOfItems = items.asSnapshot()
        assertThat(snapshotOfItems.first(), Matchers.instanceOf(Collection.Header::class.java))
    }

    @Test
    fun `last item always should be ArtObject`() = runTest(testDispatcher) {
        collectionsRepository.pushArtObjects(createArtObjectsWithAuthor("author1"))

        val items = getCollectionsUseCase.invoke()

        val snapshotOfItems = items.asSnapshot()
        assertThat(snapshotOfItems.last(), Matchers.instanceOf(Collection.ArtObject::class.java))
    }

    @Test
    fun `should add two headers for two different authors`() = runTest(testDispatcher) {
        collectionsRepository.pushArtObjects(createArtObjectsWithAuthor("author1"))
        collectionsRepository.pushArtObjects(createArtObjectsWithAuthor("author2"))

        val items = getCollectionsUseCase.invoke()

        val snapshotOfItems = items.asSnapshot()
        val headerCount = snapshotOfItems.filterIsInstance<Collection.Header>().count()
        assertEquals(2, headerCount)
    }

    @Test
    fun `should add three headers for three different authors`() = runTest(testDispatcher) {
        collectionsRepository.pushArtObjects(createArtObjectsWithAuthor("author1"))
        collectionsRepository.pushArtObjects(createArtObjectsWithAuthor("author2"))
        collectionsRepository.pushArtObjects(createArtObjectsWithAuthor("author3"))

        val items = getCollectionsUseCase.invoke()

        val snapshotOfItems = items.asSnapshot()
        val headerCount = snapshotOfItems.filterIsInstance<Collection.Header>().count()
        assertEquals(3, headerCount)
    }
}