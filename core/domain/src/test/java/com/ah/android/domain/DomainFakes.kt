package com.ah.android.domain

import com.ah.android.assignment.model.collection.Collection

val artObject = Collection.ArtObject(
    objectNumber = "1",
    title = "",
    author = "Author1",
    imageUrl = "",
)

fun createArtObjectsWithAuthor(author: String): MutableList<Collection> {
    val fakes = mutableListOf<Collection>()
    repeat(5) {
        fakes.add(
            Collection.ArtObject(
                objectNumber = "$it",
                title = "title",
                author = author,
                imageUrl = "url",
            )
        )
    }
    return fakes
}