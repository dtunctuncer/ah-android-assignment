package com.ah.android.domain

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.testing.asPagingSourceFactory
import com.ah.android.assignment.data.repository.collections.CollectionsRepository
import com.ah.android.assignment.model.collection.ArtObjectDetail
import com.ah.android.assignment.model.collection.Collection
import kotlinx.coroutines.flow.Flow

class FakeCollectionsRepository : CollectionsRepository {
    private val artObjects = mutableListOf<Collection>()

    override fun getArtObjects(): Flow<PagingData<Collection>> {
        return Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            initialKey = 0,
            pagingSourceFactory = artObjects.asPagingSourceFactory()
        ).flow
    }

    fun pushArtObjects(list: List<Collection>) {
        artObjects.addAll(list)
    }

    override fun getArtObjectDetail(objectNumber: String): Flow<ArtObjectDetail> {
        throw IllegalAccessException("getArtObjectDetail is not implemented")
    }
}