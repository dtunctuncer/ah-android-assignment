plugins {
    id("com.ah.android.assignment.library")
    id("com.ah.android.assignment.hilt")
}

android {
    namespace = "com.ah.android.assignment.core.common"
    defaultConfig {
        testInstrumentationRunner =
            "com.ah.android.assignment.testing.TaskTestRunner"
    }
    packaging {
        resources.excludes.add("META-INF/*")
    }
}

dependencies {
    implementation(libs.kotlinx.coroutines.android)
    implementation(libs.androidx.lifecycle.runtime.ktx)
    // force upgrade to 1.1.0 because its required by androidTestImplementation,
    // and without this statement AGP will silently downgrade to tracing:1.0.0
    implementation("androidx.tracing:tracing:1.1.0")
    testImplementation(project(":core:testing"))
    androidTestImplementation(project(":core:testing"))
}
