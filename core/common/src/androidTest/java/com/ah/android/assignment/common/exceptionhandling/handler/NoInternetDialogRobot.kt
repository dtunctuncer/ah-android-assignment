package com.ah.android.assignment.common.exceptionhandling.handler

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.ah.android.assignment.core.common.R

internal fun noInternetDialog(robot: NoInternetDialogRobot.() -> Unit) {
    NoInternetDialogRobot().apply {
        robot()
    }
}

internal class NoInternetDialogRobot {
    fun isShowing() {
        onView(withText(android.R.string.dialog_alert_title)).inRoot(isDialog()).check(matches(isDisplayed()))
        onView(withText(R.string.no_internet_connection_please_try_again_later)).inRoot(isDialog()).check(matches(isDisplayed()))
        onView(withText(android.R.string.ok)).inRoot(isDialog()).check(matches(isDisplayed()))
    }

    fun isNotShowing() {
        onView(withText(android.R.string.dialog_alert_title)).check(doesNotExist())
        onView(withText(R.string.no_internet_connection_please_try_again_later)).check(doesNotExist())
        onView(withText(android.R.string.ok)).check(doesNotExist())
    }

    fun pressOk() {
        onView(withText(android.R.string.ok)).inRoot(isDialog()).perform(click())
    }
}
