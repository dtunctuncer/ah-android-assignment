package com.ah.android.assignment.common.exceptionhandling.handler

import androidx.test.core.app.ActivityScenario
import com.ah.android.assignment.common.exceptionhandling.exception.BaseException
import com.ah.android.assignment.common.exceptionhandling.exception.NoInternetConnectionException
import com.ah.android.assignment.testing.EmptyActivity
import org.junit.Before
import org.junit.Test

class NoInternetConnectionExceptionHandlerTest {
    private val handler = NoInternetConnectionExceptionHandler()
    private lateinit var scenario: ActivityScenario<EmptyActivity>

    @Before
    fun setup() {
        scenario = ActivityScenario.launch(EmptyActivity::class.java)
    }

    @Test
    fun whenNoInternetExceptionIsThrownThenShowDialog() {
        scenario.onActivity { activity ->
            handler.handleException(activity, NoInternetConnectionException())
        }

        noInternetDialog {
            isShowing()
        }
    }

    @Test
    fun whenNoInternetExceptionIsThrownAndDialogIsDismissedThenDialogShouldBeHidden() {
        scenario.onActivity { activity ->
            handler.handleException(activity, NoInternetConnectionException())
        }

        noInternetDialog {
            pressOk()
            isNotShowing()
        }
    }

    @Test
    fun whenIncompatibleExceptionIsThrownThenDontShowDialog() {
        scenario.onActivity { activity ->
            handler.handleException(activity, BaseException())
        }

        noInternetDialog {
            isNotShowing()
        }
    }

    @Test
    fun whenDialogIsShownAndActivityRestartsThenDialogShouldStillBeVisible() {
        scenario.onActivity { activity ->
            handler.handleException(activity, NoInternetConnectionException())
        }

        noInternetDialog {
            induceDelay()
            isShowing()
        }

        scenario.recreate()

        noInternetDialog {
            induceDelay()
            isShowing()
        }
    }

    /**
     * Adds a sleep statement to match the app's execution delay.
     */
    private fun induceDelay() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(700)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }
}
