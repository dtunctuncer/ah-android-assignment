package com.ah.android.assignment.common.exceptionhandling

import androidx.fragment.app.FragmentActivity
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.ah.android.assignment.common.exceptionhandling.exception.BaseException
import com.ah.android.assignment.core.common.R
import com.ah.android.assignment.testing.EmptyActivity
import org.junit.Before
import org.junit.Test

class ExceptionHandlerProxyTest {
    private lateinit var scenario: ActivityScenario<EmptyActivity>

    @Before
    fun setup() {
        scenario = ActivityScenario.launch(EmptyActivity::class.java)
    }

    @Test
    fun whenNoCompatibleHandlerIsFoundThenShowFallbackErrorDialog() {
        val proxy = ExceptionHandlerProxy(emptyList())
        scenario.onActivity { activity ->
            proxy.handleException(activity, BaseException())
        }

        onView(withText(android.R.string.dialog_alert_title)).inRoot(isDialog()).check(matches(isDisplayed()))
        onView(withText(R.string.unknown_error)).inRoot(isDialog()).check(matches(isDisplayed()))
        onView(withText(android.R.string.ok)).inRoot(isDialog()).check(matches(isDisplayed()))
    }

    @Test
    fun whenHandlerReturnsTrueThenDontShowFallbackDialog() {
        val emptyHandler = object : ExceptionHandler {
            override fun handleException(activity: FragmentActivity, exception: BaseException): Boolean = true
        }
        val proxy = ExceptionHandlerProxy(emptyHandler)
        scenario.onActivity { activity ->
            proxy.handleException(activity, BaseException())
        }

        onView(withText(android.R.string.ok)).check(doesNotExist())
    }
}
