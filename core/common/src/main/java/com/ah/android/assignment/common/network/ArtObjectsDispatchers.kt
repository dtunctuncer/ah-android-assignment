package com.ah.android.assignment.common.network

import javax.inject.Qualifier
import kotlin.annotation.AnnotationRetention.RUNTIME

@Qualifier
@Retention(RUNTIME)
annotation class Dispatcher(val artObjectsDispatchers: ArtObjectsDispatchers)

enum class ArtObjectsDispatchers {
    IO,
    MAIN
}
