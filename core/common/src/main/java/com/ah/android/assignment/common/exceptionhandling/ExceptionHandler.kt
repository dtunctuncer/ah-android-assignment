package com.ah.android.assignment.common.exceptionhandling

import androidx.fragment.app.FragmentActivity
import com.ah.android.assignment.common.exceptionhandling.exception.BaseException

internal interface ExceptionHandler {

    /**
     * @param activity context in which to execute the handling of exceptions
     * @param exception the exception to handle
     * @return true if the exception has been handled
     */
    fun handleException(activity: FragmentActivity, exception: BaseException): Boolean
}