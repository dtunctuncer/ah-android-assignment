package com.ah.android.assignment.common.exceptionhandling

import androidx.fragment.app.FragmentActivity
import com.ah.android.assignment.common.exceptionhandling.exception.BaseException
import com.ah.android.assignment.common.exceptionhandling.handler.NoInternetConnectionExceptionHandler

class ExceptionHandling {
    // The global entry point for exception handling
    private val exceptionHandlerProxy: ExceptionHandlerProxy by lazy {
        ExceptionHandlerProxy(
            NoInternetConnectionExceptionHandler(),
        )
    }

    fun handleException(activity: FragmentActivity, exception: Throwable) {
        val baseException = exception.toBaseException()
        exceptionHandlerProxy.handleException(activity, baseException)
    }
}

private fun Throwable.toBaseException(): BaseException = when (this) {
    is BaseException -> this
    else -> BaseException(this)
}
