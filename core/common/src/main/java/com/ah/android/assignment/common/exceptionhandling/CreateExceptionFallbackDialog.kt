package com.ah.android.assignment.common.exceptionhandling

import com.ah.android.assignment.common.exceptionhandling.ui.ErrorDialogFragment
import com.ah.android.assignment.core.common.R

internal fun createExceptionFallbackDialog() = ErrorDialogFragment.create(R.string.unknown_error)
