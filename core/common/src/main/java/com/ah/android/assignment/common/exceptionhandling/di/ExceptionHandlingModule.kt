package com.ah.android.assignment.common.exceptionhandling.di

import com.ah.android.assignment.common.exceptionhandling.ExceptionHandling
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ExceptionHandlingModule {
    @Provides
    @Singleton
    fun provideExceptionHandling() = ExceptionHandling()
}