package com.ah.android.assignment.common.exceptionhandling

import androidx.fragment.app.FragmentActivity
import com.ah.android.assignment.common.exceptionhandling.exception.BaseException


internal class ExceptionHandlerProxy(
    private val handlers: List<ExceptionHandler>
) : ExceptionHandler {

    constructor(vararg handlers: ExceptionHandler) : this(handlers.toList())

    /**
     * All registered ExceptionHandler instances are given the exception for handling.
     * If none of them handle this exception, a default error dialog is shown.
     */
    override fun handleException(activity: FragmentActivity, exception: BaseException): Boolean {
        val handled = handlers.count { handler ->
            handler.handleException(activity, exception)
        }

        if (handled == 0) {
            applyFallbackErrorHandling(activity)
        }

        return true
    }

    /**
     * Show a default error dialog for all exceptions resulting from exceptions
     * that have not been handled by any specific ExceptionHandler.
     */
    private fun applyFallbackErrorHandling(activity: FragmentActivity?) {
        // default error handling
        if (activity != null && !activity.isFinishing && !activity.isDestroyed) {
            createExceptionFallbackDialog().show(activity.supportFragmentManager, "FallbackErrorDialog")
        }
    }
}
