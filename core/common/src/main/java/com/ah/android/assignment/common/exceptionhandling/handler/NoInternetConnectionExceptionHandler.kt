package com.ah.android.assignment.common.exceptionhandling.handler

import androidx.fragment.app.FragmentActivity
import com.ah.android.assignment.common.exceptionhandling.ExceptionHandler
import com.ah.android.assignment.common.exceptionhandling.exception.BaseException
import com.ah.android.assignment.common.exceptionhandling.exception.NoInternetConnectionException
import com.ah.android.assignment.common.exceptionhandling.ui.ErrorDialogFragment
import com.ah.android.assignment.core.common.R

internal class NoInternetConnectionExceptionHandler : ExceptionHandler {
    override fun handleException(activity: FragmentActivity, exception: BaseException): Boolean {
        return if (exception is NoInternetConnectionException) {
            onNoInternetConnectionException(activity)
            true
        } else {
            false
        }
    }

    private fun onNoInternetConnectionException(activity: FragmentActivity) {
        ErrorDialogFragment.create(
            R.string.no_internet_connection_please_try_again_later
        ).show(activity.supportFragmentManager, "NoInternetConnectionException")
    }
}
