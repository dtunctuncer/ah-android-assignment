package com.ah.android.assignment.common.exceptionhandling.ui

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment

class ErrorDialogFragment : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = AlertDialog.Builder(requireContext())
        .setTitle(android.R.string.dialog_alert_title)
        .setMessage(getString(requireArguments().getInt(ARG_MESSAGE, 0)))
        .setPositiveButton(getString(android.R.string.ok)) { _, _ -> }
        .create()

    companion object {
        private const val ARG_MESSAGE = "ARG_MESSAGE"
        fun create(@StringRes messageId: Int): ErrorDialogFragment {
            return ErrorDialogFragment().apply { arguments = bundleOf(ARG_MESSAGE to messageId) }
        }
    }
}