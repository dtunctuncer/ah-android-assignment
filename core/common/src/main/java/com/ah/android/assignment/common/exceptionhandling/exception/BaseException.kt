package com.ah.android.assignment.common.exceptionhandling.exception

open class BaseException : Exception {

    constructor() : super()

    constructor(throwable: Throwable) : super(throwable)

    constructor(detailMessage: String) : super(detailMessage)

    constructor(detailMessage: String, throwable: Throwable) : super(detailMessage, throwable)
}