package com.ah.android.assignment.common

import app.cash.turbine.test
import com.ah.android.assignment.common.result.Result
import com.ah.android.assignment.common.result.asResult
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test

class ResultTest {
    @Test
    fun `asResult shows correct states`() = runTest {
        val successResult = "result"
        val exceptionMessage = "operation is failed"
        flow {
            emit(successResult)
            throw Exception(exceptionMessage)
        }
            .asResult()
            .test {
                assertEquals(Result.Loading, awaitItem())
                assertEquals(Result.Success(successResult), awaitItem())
                when (val item = awaitItem()) {
                    is Result.Error -> assertEquals(item.exception?.message, exceptionMessage)
                    else -> throw IllegalStateException("Flow should have emitted an error")
                }
                awaitComplete()
            }
    }
}
