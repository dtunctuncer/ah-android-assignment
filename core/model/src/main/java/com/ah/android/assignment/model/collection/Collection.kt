package com.ah.android.assignment.model.collection


sealed class Collection {
    data class ArtObject(
        val objectNumber: String,
        val title: String,
        val author: String,
        val imageUrl: String?
    ) : Collection()

    data class Header(val description: String) : Collection()
}
