package com.ah.android.assignment.model.collection

data class ArtObjectDetail(
    val objectNumber: String,
    val title: String,
    val longTitle: String,
    val subtitle: String,
    val imageUrl: String?,
    val description: String?,
    val objectTypes: String,
    val objectCollection: String,
    val maker: String,
    val materials: String,
)
