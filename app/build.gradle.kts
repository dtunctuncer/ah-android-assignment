@file:Suppress("UnstableApiUsage")

plugins {
    id("com.ah.android.assignment.application")
    id("com.ah.android.assignment.hilt")
}

android {
    namespace = "com.ah.android.assignment"

    defaultConfig {
        applicationId = "com.ah.android.assignment"
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "com.ah.android.assignment.testing.TaskTestRunner"
    }

    buildTypes {
        val debug by getting {
            isDebuggable = true
        }
        val release by getting {
            isMinifyEnabled = true
            isCrunchPngs = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    testOptions {
        unitTests {
            isIncludeAndroidResources = true
        }
    }

    buildFeatures {
        viewBinding = true
    }

    kapt {
        correctErrorTypes = true
    }

    packaging {
        resources.excludes.add("META-INF/*")
    }
}

dependencies {
    implementation(project(":core:common"))
    implementation(project(":core:model"))

    implementation(project(":feature:art-objects"))
    implementation(project(":feature:art-object-detail"))

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.fragment)
    implementation(libs.material)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.coil.kt)
    implementation(libs.androidx.navigation.fragment.ktx)
    implementation(libs.androidx.navigation.ui.ktx)

    testImplementation(libs.junit4)

    androidTestImplementation(project(":core:testing"))
}
